import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import routers from './Routers';
import { BrowserRouter as Router, Link} from "react-router-dom";
import registerServiceWorker from './registerServiceWorker';


ReactDOM.render(
	<Router>
    {routers}
	</Router>,
	document.getElementById('root')
	);
registerServiceWorker();
