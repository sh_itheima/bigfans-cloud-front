/**
 * Created by lichong on 2/27/2018.
 */
import React from 'react';

import {Row, Col, Menu, Icon, Divider} from 'antd';
import MyAddress from './MyAddress'
import MyOrders from './MyOrders'
import MyCoupons from './MyCoupons'


class Center extends React.Component {

    state = {
        currentMenu: 'myorders'
    }

    render() {
        return (
            <div>
                {
                    this.state.currentMenu == 'myorders'
                    &&
                    <MyOrders/>
                }
                {
                    this.state.currentMenu == 'myaddress'
                    &&
                    <MyAddress/>
                }
                {
                    this.state.currentMenu == 'mycoupons'
                    &&
                    <MyCoupons/>
                }
            </div>
        )
    }
}

export default Center;